%Getting the atomdata in the loop
n=length(xcoordinates);
a=5;
m=a^3;
for k=1:n
    x1=xcoordinates(k);
    y1=ycoordinates(k);
    z1=zcoordinates(k);
I_11=m*(y1^.2+z1^.2+(2/5)*a^2);
I_22=m*(x1^.2+z1^.2+(2/5)*a^2);
I_33=m*(x1^.2+y1^.2+(2/5)*a^2);
I_12=-(m*x1*y1);
I_21=I_12;
I_13=-(m*x1*z1);
I_31=I_13;
I_23=-(m*y1*z1);
I_32=I_23;
A=[I_11,I_12,I_13; I_21,I_22,I_23; I_31,I_32,I_33];
lambda = eig(A);
sort(lambda);
I_xx=min(lambda);
I_yy=median(lambda);
I_zz=max(lambda);
semiaxes_a=sqrt((5/(2*m))*(-I_xx+I_yy+I_zz))
semiaxes_b=sqrt((5/(2*m))*(I_xx-I_yy+I_zz))
semiaxes_c=sqrt((5/(2*m))*(I_xx+I_yy-I_zz))
end
