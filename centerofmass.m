%Moving ellipsoid to center of mass
%mass of each atom
m=[atomradii.^3];
%Total mass
M=sum(m);

% I just ran atomdata
xcoordinate = xcoordinates';
ycoordinate = ycoordinates';
zcoordinate = zcoordinates';

%Center of mass
m_r=[m(1)*xcoordinate(1) + m(1)*ycoordinate(1) + m(1)*zcoordinate(1)+ m(2)*xcoordinate(2) + m(2)*ycoordinate(2) + m(2)*zcoordinate(2)+ m(3)*xcoordinate(3)+ m(3)*ycoordinate(3)+ m(3)*ycoordinate(3)];
r_c=M^(-1)*(m_r);


r_cx=[xcoordinate]-r_c;
r_cy=[ycoordinate]-r_c;
r_cz=[zcoordinate]-r_c;
%coordinates of center of mass
r_i=[r_cx;r_cy;r_cz];



%%%%
% center of mass, written in matrix and vector form
% each column of r is one point
r = [xcoordinate;
     ycoordinate;
     zcoordinate;];

% m_r adds each column scaled by the mass
m_r = r*m;

% M is a scalar 
r_c2 = M^(-1)*m_r; % we could also write m_r / M because M is a scalar


r_centered = r - r_c2*[1 1 1]; % check out what r_c2*[1 1 1] is
%%%% 

