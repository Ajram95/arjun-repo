with open('phe_solvated.txt', 'r') as f:
    lines = f.readlines()

with open('phe_solvated.txt', 'w') as f:
    for line in lines:
        line = line.replace('0.51000','00000000') #just first atom, but discharges every atom with same charge which we do not want
        f.write(line) #will make a loop for writing into directories for each discharged atom, basically 32 directories (remaining are water)
