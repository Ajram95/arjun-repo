from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst, new_file_path):
    #Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                # new_file.write(line.replace(pattern, subst))
                if pattern in line : 
                    print(line);
                    new_file.write(line.replace(pattern, subst));
                else :
                    new_file.write(line);

    close(fh)
    
    move(abs_path, (new_file_path))

replace("phe_solvated.txt", "0.51", "0.00", "1phe_solvated.txt.psf")
