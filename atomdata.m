%Code for loading atom data
filename = '../lab-wide/jbardhan/test1.xyzr';
formatSpec = '%11f%9f%9f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', '', 'WhiteSpace', '',  'ReturnOnError', false);
fclose(fileID);
xcoordinates = dataArray{:, 1};
ycoordinates = dataArray{:, 2};
zcoordinates = dataArray{:, 3};
atomradii = dataArray{:, 4};
clearvars filename formatSpec fileID dataArray ans;